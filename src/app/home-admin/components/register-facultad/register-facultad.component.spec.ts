import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterFacultadComponent } from './register-facultad.component';

describe('RegisterFacultadComponent', () => {
  let component: RegisterFacultadComponent;
  let fixture: ComponentFixture<RegisterFacultadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterFacultadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterFacultadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
