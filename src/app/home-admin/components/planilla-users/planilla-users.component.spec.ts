import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanillaUsersComponent } from './planilla-users.component';

describe('PlanillaUsersComponent', () => {
  let component: PlanillaUsersComponent;
  let fixture: ComponentFixture<PlanillaUsersComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanillaUsersComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanillaUsersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
