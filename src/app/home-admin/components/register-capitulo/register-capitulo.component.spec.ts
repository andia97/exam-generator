import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterCapituloComponent } from './register-capitulo.component';

describe('RegisterCapituloComponent', () => {
  let component: RegisterCapituloComponent;
  let fixture: ComponentFixture<RegisterCapituloComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterCapituloComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterCapituloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
