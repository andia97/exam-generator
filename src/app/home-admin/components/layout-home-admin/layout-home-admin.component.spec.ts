import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LayoutHomeAdminComponent } from './layout-home-admin.component';

describe('LayoutHomeAdminComponent', () => {
  let component: LayoutHomeAdminComponent;
  let fixture: ComponentFixture<LayoutHomeAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LayoutHomeAdminComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LayoutHomeAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
