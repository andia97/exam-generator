import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LayoutHomeAdminComponent } from './components/layout-home-admin/layout-home-admin.component';
import { PlanillaUsersComponent } from './components/planilla-users/planilla-users.component';
import { RegisterCarreraComponent } from './components/register-carrera/register-carrera.component';
import { RegisterFacultadComponent } from './components/register-facultad/register-facultad.component';
import { RegisterUsersComponent } from './components/register-users/register-users.component';

const routes: Routes = [
  {
    path: '',
    component: LayoutHomeAdminComponent,
    children: [
      { path: '', pathMatch: 'full', redirectTo: 'users' },
      { path: 'users', component: PlanillaUsersComponent},
      { path: 'register-user', component: RegisterUsersComponent},
      { path: 'register-career', component: RegisterCarreraComponent},
      { path: 'register-faculty', component: RegisterFacultadComponent},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeAdminRoutingModule { }
