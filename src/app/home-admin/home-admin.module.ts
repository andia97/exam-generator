import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeAdminRoutingModule } from './home-admin-routing.module';
import { SharedModule } from '../shared/shared.module';

import { RegisterUsersComponent } from './components/register-users/register-users.component';
import { RegisterRolesComponent } from './components/register-roles/register-roles.component';
import { PlanillaUsersComponent } from './components/planilla-users/planilla-users.component';
import { RegisterFacultadComponent } from './components/register-facultad/register-facultad.component';
import { RegisterCarreraComponent } from './components/register-carrera/register-carrera.component';
import { RegisterMateriaComponent } from './components/register-materia/register-materia.component';
import { RegisterCapituloComponent } from './components/register-capitulo/register-capitulo.component';
import { LayoutHomeAdminComponent } from './components/layout-home-admin/layout-home-admin.component';



@NgModule({
  declarations: [
    // RegisterUsersComponent,
    // RegisterRolesComponent,
    // PlanillaUsersComponent,
    // RegisterFacultadComponent,
    // RegisterCarreraComponent,
    // RegisterMateriaComponent,
    // RegisterCapituloComponent,
    LayoutHomeAdminComponent
  ],
  imports: [
    CommonModule,
    HomeAdminRoutingModule,
    SharedModule
  ]
})
export class HomeAdminModule { }
