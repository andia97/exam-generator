import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';

const routes: Routes = [
  {path:'', redirectTo:'login', pathMatch:'full'},
  { 
    path: 'login', 
    component: LoginComponent
  },
  {
    path: 'admin',
    loadChildren: () => import('./home-admin/home-admin.module').then(m => m.HomeAdminModule)
  },
  {
    path: 'docente',
    loadChildren: () => import('./home-docente/home-docente.module').then(m => m.HomeDocenteModule)
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
