import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {FormControl, FormGroup,Validators} from '@angular/forms';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})


export class LoginComponent {
    mandoForm = new FormGroup({
    email: new FormControl(''),
    password:new FormControl('')
  });
  
  email = new FormControl('', [Validators.required, Validators.email]);

  submit() {
    if (this.mandoForm.valid) {
      console.log(this.mandoForm.value)
    }
    else{
      alert("Rellenar todos los campos")
    }
  }
}


