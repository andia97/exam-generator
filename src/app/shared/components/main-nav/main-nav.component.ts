import { Component, OnInit } from '@angular/core';

export interface RouteList {
  route: string;
  label: string;
  icon: string;
}

@Component({
  selector: 'app-main-nav',
  templateUrl: './main-nav.component.html',
  styleUrls: ['./main-nav.component.css']
})
export class MainNavComponent implements OnInit {
  routeList: RouteList[] = [
    {
      route: 'users',
      label: 'Lista de usuarios',
      icon: 'home'
    },
    {
      route: 'register-user',
      label: 'Registrar usuarios',
      icon: 'filter'
    },
  ]
  constructor() { }

  ngOnInit(): void {
  }

}
