import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanillaPreguntasComponent } from './planilla-preguntas.component';

describe('PlanillaPreguntasComponent', () => {
  let component: PlanillaPreguntasComponent;
  let fixture: ComponentFixture<PlanillaPreguntasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PlanillaPreguntasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanillaPreguntasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
