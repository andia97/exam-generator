import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeDocenteRoutingModule } from './home-docente-routing.module';
import { CrearExamenComponent } from './components/crear-examen/crear-examen.component';
import { AgregarPreguntaComponent } from './components/agregar-pregunta/agregar-pregunta.component';
import { PlanillaPreguntasComponent } from './components/planilla-preguntas/planilla-preguntas.component';


@NgModule({
  declarations: [
    CrearExamenComponent,
    AgregarPreguntaComponent,
    PlanillaPreguntasComponent
  ],
  imports: [
    CommonModule,
    HomeDocenteRoutingModule
  ]
})
export class HomeDocenteModule { }
